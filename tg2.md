<!DOCTYPE html>
<html>

<head lang="es">
	<meta charset="utf-8">
	<title>Trabajo en grupo TG2</title>
	<link rel="stylesheet" href="style.css">
	<style>
		table,
		th,
		td {
			border: 1px solid black;
		}
	</style>
</head>

<body>
	<header>
		<h1>Desarrollo con Tecnologias Emergentes. Curso 2023-24<br>
			Trabajo TG2 del grupo ??<br>
			Titulo: TG2. Evaluar y comparar dos tecnologias emergentes</h1>
	</header>
	<nav>
		<ul>
			<li><a href="#1">1. Informacion general</a></li>
			<li><a href="#2">2. Descripcion de las tecnologias</a></li>
			<li><a href="#3">3. Criterios de comparacion</a></li>
			<li><a href="#4">4. Evaluacion de los criterios por categoria</a></li>
			<li><a href="#5">5. Comparacion de las tecnologias</a></li>
			<li><a href="#6">6. Recomendaciones</a></li>
		</ul>
	</nav>
		<section id=1">
			<h2>1. Informacion general</h2>
			<h3>1.1 Autores</h3>
			<h3>1.2 Proyecto Scrum y repositorio en GitLab</h3>
			<h3>1.3 Burndown chart</h3>
			<img src="burndownTG2.jpg">
			<h3>1.4 Presentacion del trabajo</h3>
			<a href="https://URL">Presentacion con diapositivas del trabajo TG2</a></p>
			<h2>2. Descripcion de las tecnologias</h2>
			<h2>3. Criterios de comparacion</h2>
			<h3>3.1 Categoria A: General</h3>
			<h4>3.1.1 Criterio A.1: Autor de la herramienta de modelado UML</h4>
			<ul>
				<li>Nombre del criterio: Autor.</li>
				<li>Descripcion: Nombre de la persona, institucion o empresa que ha creado la herramienta de modelado
					UML.</li>
				<li>Tipo de valor: Texto libre.</li>
			</ul>
			<h4>3.1.2 Criterio A.2: ...</h4>
			<ul>
				<li>Nombre del criterio: ...</li>
				<li>Descripcion: ...</li>
				<li>Tipo de valor: ...</li>
			</ul>
			....
			<h3>3.2 Categoria B: Utilidades</h3>
			<h4>3.2.1 Criterio B.1: Generacion de codigo Java</h4>
			<ul>
				<li>Nombre del criterio: Generacion de codigo Java.</li>
				<li>Descripcion: Indica si la herramienta de modelado UML incluye funcionalidad para generar archivos
					fuente .java a partir de diagramas de clase.</li>
				<li>Tipo de valor: Booleano (Si/No).</li>
			</ul>
			<h4>3.2.2 Criterio B.2: Numero de proyectos simultaneos</h4>
			<ul>
				<li>Nombre del criterio: Proyectos simultaneos.</li>
				<li>Descripcion: Indica el numero maximo de proyectos de modelado que deja tener abiertos la herramienta
					de forma simultanea.</li>
				<li>Tipo de valor: Numerico.</li>
			</ul>
			....
			<h3>3.3 Categoria C: ...</h3>
			...
			<h3>3.4 Categoria D: ...</h3>
			...
			<h2>4. Evaluacion de los criterios por categoria</h2>
			<h3>4.1 Evaluacion de los criterios para la tecnologia 1</h3>
			<p>Debe incluir al menos una tabla con la siguiente estructura.</p>
			<table>
				<tr>
					<th>Criterio</th>
					<th>Evaluacion</th>
					<th>Fuente</th>
				</tr>
				<tr>
					<td>Criterio A.1: Nombre</td>
					<td>Valor</td>
					<td>URL de la fuente donde se ha encontrado el valor para el criterio</td>
				</tr>
				<tr>
					<td>Criterio A.2: Nombre</td>
					<td>Valor</td>
					<td>URL de la fuente donde se ha encontrado el valor para el criterio</td>
				</tr>
				<tr>
					<td>Criterio ...</td>
					<td>Valor</td>
					<td>URL de la fuente donde se ha encontrado el valor para el criterio</td>
				</tr>
			</table>
			<h3>4.2 Evaluacion de los criterios para la tecnologia 2</h3>
			<table>
				<tr>
					<th>Criterio</th>
					<th>Evaluacion</th>
					<th>Fuente</th>
				</tr>
				<tr>
					<td>Criterio A.1: Nombre</td>
					<td>Valor</td>
					<td>URL de la fuente donde se ha encontrado el valor para el criterio</td>
				</tr>
				<tr>
					<td>Criterio A.2: Nombre</td>
					<td>Valor</td>
					<td>URL de la fuente donde se ha encontrado el valor para el criterio</td>
				</tr>
				<tr>
					<td>Criterio ...</td>
					<td>Valor</td>
					<td>URL de la fuente donde se ha encontrado el valor para el criterio</td>
				</tr>
			</table>
			<h2>5. Comparacion de las tecnologias</h2>
			<p>Debe incluir al menos una tabla resumen cruzando los criterios y los
				valores de cada tecnología. Con una columna de comentarios sobre la comparacion.</p>
			<p>Esta tabla es obligatoria, aunque se pueden incluir
				otros graficos o tablas complementarias copiadas y pegadas desde diversas fuentes de informacion,
				siempre que debajo de cada uno se indique la fuente (al menos la URL).</p>
			<table>
				<tr>
					<th>Criterio</th>
					<th>Tecnologia 1</th>
					<th>Tecnologia 2</th>
					<th>Comentarios</th>
				</tr>
				<tr>
					<td>A.1</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			<h2>6. Recomendaciones</h2>
			<p>Deben platearse posibles situaciones de uso, y recomendar justificadamente una u otra tecnología en
				funcion de la situacion. Al menos 2 situaciones diferentes.</p>
			<h3>6.1 Situacion A</h3>
			<h4>6.1.1 Descripcion de la situacion</h4>
			<p>Breve descripcion de la situacion. Por ejemplo, una posible situacion en el caso de comparar dos
				herramientas de modelado UML, podría ser el caso de una empresa de desarrollo muy interesada en
				tecnologías open
				source, que programa solo en Java, con equipos de desarrollo pequeños, que utiliza UML como notacion,
				etc..."</p>
			<h4>6.1.2 Recomendacion de la tecnología a utilizar</h4>
			<p>Debe indicarse la tecnología propuesta para esa situacion.</p>
			<p>Debe incluirse una tabla como la siguiente, mostrando las ventajas, respecto a los criterios, que ofrece
				cada tecnología en esa situacion concreta.</p>
			<p>Incluir solo los criterios sobre los que se aprecien ventajas de una de las tecnologías frente a otra. No
				incluir criterios que no sean relevantes para la decision (por ejemplo, el criterio “autor” seguramente
				no sera relevante).</p>
			<p>Hay que razonar la recomendación, no es suficiente con incluir la tabla.</p>
			<table>
				<tr>
					<th>Criterios relevantes para la decision</th>
					<th>Ventajas tecnología 1</th>
					<th>Ventajas tecnología 2</th>
				</tr>
				<tr>
					<td>...</td>
					<td></td>
					<td></td>
				</tr>
			</table>
			<h3>6.2 Situacion B</h3>
			<h4>6.2.1 Descripcion de la situacion</h4>
			<p>Breve descripcion de la situacion. Por ejemplo, una posible situacion en el caso de comparar dos
				herramientas de modelado UML, podría ser el caso de una empresa de desarrollo muy interesada en
				tecnologías open
				source, que programa solo en Java, con equipos de desarrollo pequeños, que utiliza UML como notacion,
				etc..."</p>
			<h4>6.2.2 Recomendacion de la tecnología a utilizar</h4>
			<table>
				<tr>
					<th>Criterios relevantes para la decision</th>
					<th>Ventajas tecnología 1</th>
					<th>Ventajas tecnología 2</th>
				</tr>
				<tr>
					<td>...</td>
					<td></td>
                    <td></td>
				</tr>
			</table>
</body>
</html>